
#ifndef HY16F391APB_H_
#define HY16F391APB_H_


#include <stdint.h>



extern volatile uint32_t INTCOM ;//0x40000))
//extern volatile uint32_t INTCOM;
//asm (".equ INTCOM, 0x40000");
/*
 * INT Base Address + 0x00 (0x40000)
Symbol INTCOM (Interrupt Control Register 0)
Bit [31:24] [23:22] [21] [20] [19] [18] [17] [16]
名稱 MASK - I2CEIE I2CIE UTxIE URxIE STxIE SRxIE
RW R0W-0 - RW-0
Bit [15:14] [13] [12] [11] [10] [09] [08] [07:06] [05] [04] [03] [02] [01] [00]
名稱
MASK - I2CEIF I2CIF UTxIF URxIF STxIF SRxIF
- I2CEIR I2CIR UTxIR URxIR STxIR SRxIR
RW R-0 - RW0-0
 */
extern volatile uint16_t INTCOMH ;// 0x40002))
extern volatile uint16_t INTCOML ;// 0x40000))



extern volatile uint32_t INTTMR ;//0x40004))
/*
INT Base Address + 0x04 (0x40004)
Symbol INTTMR (Interrupt Control Register 1)
Bit [31:24] [23:22] [21] [20] [19] [18] [17] [16]
名稱 MASK - RTCIE WDTIE TMC1IE TMC0IE TMBIE TMAIE
RW R0W-0 - RW-0
Bit [15:14] [13] [12] [11] [10] [09] [08] [07:06] [05] [04] [03] [02] [01] [00]
名稱 MASK - RTCIR WDTIR TMC1IR TMC0IR TMBIR TMAIR - RTCIF WDTIF TMC1IF TMC0IF TMBIF TMAIF
RW R0W-0 - RW0-0
*/

extern volatile uint32_t INTADC ;//0x40008))
/*
 * INT Base Address + 0x08 (0x40008)
Symbol INTADC (Interrupt Control Register 2)
Bit [31:24] [23:17] [16]
名稱 MASK Rsv ADCIE
RW R0W-0 R-0 RW-0
Bit [15:9] [8] [07:01] [00]
名稱 Rsv ADCIR Rsv ADCIF
RW R-0 R-0 R-0
 */
extern volatile uint32_t INTLVD ;//0x4000C))
/*
 * INT Base Address + 0x0C (0x4000C)
Symbol INTLVD (Interrupt Control Register 3)
Bit [31:24] [23:18] [17] [16]
名稱 MASK Rsv BOR2IE LVDIE
RW R0W-0 R-0 RW-0 RW-0
Bit [15:10] [09] [08] [07:02] [01] [00]
名稱
MASK Rsv BOR2IR LVDIR Rsv BOR2IF LVDIF
RW R-0 R-0 RW0-X RW0-0
 */





extern volatile uint32_t INTPT1 ;//0x40010))
// INTPT1
/*
INT Base Address + 0x10 (0x40010)
Symbol INTPT1 (Interrupt Control Register 4)
Bit [31:24] [23] [22] [21] [20] [19] [18] [17] [16]
名稱 MASK PT17IE PT16IE PT15IE PT14IE PT13IE PT12IE PT11IE PT10IE
RW R0W-0 RW-0
Bit [15:08] [07] [06] [05] [04] [03] [02] [01] [00]
名稱
MASK
PT3 IR PT17IF PT16IF PT15IF PT14IF PT13IF PT12IF PT11IF PT10IF
RW R-0 RW0-0
*/

extern volatile uint32_t INTPT2 ;//0x40014))
/*
 * INTPT2 (Interrupt Control Register 5)
Bit [31:24] [23] [22] [21] [20] [19] [18] [17] [16]
名稱 MASK PT27IE PT26IE PT25IE PT24IE PT23IE PT22IE PT21IE PT20IE
RW R0W-0 RW-0
Bit [15:08] [07] [06] [05] [04] [03] [02] [01] [00]
名稱
MASK PT2 IR PT27IF PT26IF PT25IF PT24IF PT23IF PT22IF PT21IF PT20IF
RW R0W-0 RW0-0
 */
extern volatile uint32_t INTUART2 ;//0x40018))
/*
 * INTUART2 (Interrupt Control Register 6)
Bit [31:24] [23:20] [19] [18] [17:16]
名稱 MASK - U2TxIE U2RxIE -
RW R0W-0 - RW-0 -
Bit [15:12] [11] [10] [09:08] [07:04] [03] [02] [01:00]
名稱
MASK
- U2TxIR U2RxIR - - U2TxIF U2RxIF -
RW R-0 - RW-0
 */

extern volatile uint32_t INTTMB2 ;//0x4001C))
/*
 * INTTMB2 (Interrupt Control Register 7)
Bit [31:24] [23:18] [17] [16]
名稱 MASK - TMB2IE -
RW R0W-0 - RW-0 -
Bit [15:10] [09] [08] [07:02] [01] [00]
名稱
MASK
- TMB2IR - - TMB2IF -
RW R-0 - RW-0
 */

extern volatile uint32_t INTPT3 ;//0x40020))
/*
 * INTPT3 (Interrupt Control Register 8)
Bit [31:24] [23] [22] [21] [20] [19] [18] [17] [16]
名稱 MASK PT37IE PT36IE PT35IE PT34IE PT33IE PT32IE PT31IE PT30IE
RW R0W-0 RW-0
Bit [15:08] [07] [06] [05] [04] [03] [02] [01] [00]
名稱
MASK
PT3 IR PT37IF PT36IF PT35IF PT34IF PT33IF PT32IF PT31IF PT30IF
RW R-0 RW0-0
 */


extern volatile uint32_t SOCSTATUS ;//0x40104))
extern volatile uint16_t SOCSTATUSL ;// 0x40104))
extern volatile uint32_t WDTCR ;//0x40108))


// SOCRST write 0x5a, we shall reset!!
extern volatile uint8_t SOCRST ;// 0x4010C))
/*
 * IWDTCR (WDT Control Register )
Bit [31] [30:16]
名稱 - WDTO
RW - R-0
Bit [15] [14:08] [07] [06] [05] [04] [03] [02:00]
名稱 - MASK - WDNMI CLRWDT ENWDT - WDTP
RW - R0W-0 - RW1-0 RW-0 RW1-0 - RW-7
 */




// PMU0 add P15 trim

extern volatile uint32_t PMU0 			;//0x40400))
/*
 * Power Management Base Address + 0x00 (0x40400)
Symbol PMU0 (PMU Control Register 0 )
Bit [31:24] [23] [22] [21:20] [19:18] [17:16]
NAME: MASK V15TRMEN - V15S VDAS ENVA
RW R0W-0 RW-0
Bit [15:08] [7] [6] [5] [4] [3] [2] [1] [0]
NAME: MASK OLVD V15PG V15OVD ENBGR ACMS Rsv ENRFO V15LP
 */


extern volatile uint32_t PMU1 			;//0x40408))
extern volatile uint32_t PMU1L 			;// 0x40408))

/*
 * Power Management Base Address + 0x08 (0x40408)
Symbol PMU1 (PMU Control Register 1)
Bit [31:19] [16]
名稱 Rsv LVDO
RW R-0 R-X
Bit [15:08] [7:4] [3] [2] [1] [0]
名稱 MASK LVDS LVDITT LVD12 LVDVS ENLVD
RW R0W-0 RW-0 RW-0 RW-0
 */





extern volatile uint32_t PMU2 			;//0x4040C))
/*
 * Power Management Base Address + 0x0C (0x4040C)
Symbol PMU2 (PMU Control Register 2)
Bit [31:24] [23:19] [18:16]
名稱 MASK - BOR2TRM
RW R0W-0 WR-0 WR-100
Bit [15:08] [7] [6:4] [3] [2] [1] [0]
名稱 MASK - BOR2TH Rsv BOR2LV BOR2S BOR2EN
RW R0W-0 - RW-0 R-0 R-X RW-1
 */


//#define DBGREG      ;//0x40100))
extern  volatile uint32_t DBGREG ;
//asm (".equ DBGREG,0x40100");
extern volatile uint16_t DBGREGH;// 0x40102))
extern volatile uint16_t DBGREGL;// 0x40100))
extern volatile uint32_t SOC; // 0x40104))


/*
 * SOC Status Base Address + 0X04 (0X40104)
Symbol SOC Status Register
Bit [31:24] [23:16]
名稱 ICE Configuration SOC Configuration
RW R-0X0F R-0X1A
Bit [15:8] [7] [6] [5] [4] [3] [2] [1] [0]
名稱 MASK - FPRG FCRst IDLE FSLP/IDLE FWDog FRST FBOR1
RW R0W-0 - RW0-0 RW0-1

note IDLE is bit 4, SLEEP is bit 3

 */

extern volatile uint32_t CLKCR0 ;//0x40300))

/*Clock Base Address + 0x00 (0x40300)
Symbol CLK0 (Clock Control Register 0)
Bit [31:16]
名稱 Rsv
RW R-0
Bit [15:8] [7] [6] [5] [4:3] [2] [1] [0]
名稱 MASK OHS_HS CKLS CKHS HAO ENOLS ENOHS ENHAO
RW R0W-0 RW-0 RW-1
*/


extern volatile uint32_t CLKCR1 ;//0x40304))

/*
Clock Base Address + 0X04 (0X40304)
Symbol CLKCR1 (Clock Control Register 1)
Bit [31:16]
名稱 Reserved
RW R-0
Bit [15:7] [6:0]
名稱 Reserved HAOTR
RW R-0 RW-0X40
*/

extern volatile uint32_t CLKCR2 ;//0x40308))

/*
Clock Base Address + 0x08 (0x40308)
Symbol CLKCR2 (Clock Control Register 2)
Bit [31:24] [23:22] [21] [20] [19] [18:16]
名稱 MASK RTCKS TUCKS ENMCD UACD
RW R0W-0 RW-0 RW-0 RW-0 RW-0
Bit [15:08] [7:6] [5:4] [3:2] [1] [0]
名稱 MASK TBCKS TBCD TACKS ENUD MCUCKS
RW R0W-0 RW-0
*/


extern volatile uint32_t CLKCR3 ;//0x4030C))

/*
Clock Base Address + 0x0C (0x4030C)
Symbol CLKCR3 (Clock Control Register 3)
Bit [31:24] [23:21] [20] [19:16]
名稱 MASK - IOCKS IOCD
RW R0W-0 - RW-0
Bit [15:08] [7] [6:4] [3] [2:0]
名稱 MASK - ADCD ENSD SPCD
RW R0W-0 R-0 RW-0
*/


extern volatile uint32_t CLKCR4 ;//0x40310))

/*
Clock Base Address + 0x10 (0x40310)
Symbol CLKCR4 (Clock Control Register 4)
Bit [31:24] [23:22] [21] [20] [19] [18:16]
名稱 MASK LCDCPD TU2CKS ENU2D - UA2CD
RW R0W-0 RW-0 - RW-0
Bit [15:08] [7] [6:4] [3:1] [0]
名稱 MASK - LCDO LCDE LCKS
RW R0W-0 - RW-0
*/

extern volatile uint32_t CLKCR5 ;//0x40314))

/*
Clock Base Address + 0x014 (0x40314)
Symbol CLKCR5 (Clock Control Register 4)
Bit [31:24] [23:18] [17] [16]
名稱 MASK - SPICKS IICCKS
RW R0W-0 R-0 RW-0 RW-0
Bit [15:8] [7:6] [5:4] [3:0]
名稱 MASK TB2CKS TM2CD -
RW R0W-0 RW-0
*/

extern volatile uint32_t CLKCR6 ;//0x40320))
extern volatile uint32_t CLKCR7 ;//0x40324))
extern volatile uint32_t CLKCR8 ;//0x40328))

extern volatile uint32_t TMACR ;//0x40C00))

/*
TMACR(TMA Control Register)
Bit [31:16]
名稱 TMAR
RW R-0
Bit [15:8] [07:06] [05] [04] [03:00]
名稱 MASK - ENTA TACLR TMAS
RW R0W-0 - RW-0 RW-0XF
*/



extern volatile uint32_t TMBCR0 ;//0x40C04))
extern volatile uint32_t TMB2CR0 ;//0x40C24))


/*
TMBCR0(TMB Control Register 0)
Bit [31:24] [23] [22:20] [19] [18:16]
名稱 MASK O1PMR O1MD O0PMR O0MD
RW R0W-0 RW-0
Bit [15:08] [7:6] [05] [04] [03:02] [01:00]
名稱 MASK - TBEN TBRST TBM TBEBS
RW R0W-0 - RW-0
*/

extern volatile uint32_t TMBCR1 ;//0x40C08))
extern volatile uint32_t TMB2CR1 ;//0x40C28))


/*
MBCR1(TMB Control Register 1)
Bit [31:22] [21] [20] [19] [18] [17] [16]
名稱 - PWMF PWME PWMD PWMC PWMB PWMA
RW - R-X
Bit [15:00]
名稱 TMBR
RW R-X
位元 名稱 描述
Bit[21-16] PWM Flag
PWM A/B/C/D/E/F 工作模式狀態旗標
0 正常
1 啟用
Bit[15-00] TMBR Timer B 16-bit 計數值
*/


// COD is 16 bits only
extern volatile uint32_t TMBCOD ;// 0x40C0C))
extern volatile uint32_t TMB2COD ;// 0x40C2C))

/*
TMBCOD(TMB Counter Overflow Condition Register )
Bit [31:16]
名稱 -
RW -
Bit [15:00]
名稱 TBC0:Timer B Overflow Condition
RW RW-0XFFFF
*/

extern volatile uint32_t PWMDOD ;//0x40C10))
extern volatile uint32_t PWM2DOD ;//0x40C30))


/*
PWMDOD(PWM Counter Overflow Condition Control Register )
Bit [31:16]
名稱 TBC2: PWM1 占空比 計數溢出值
RW RW-0XFFFF
Bit [15:00]
名稱 TBC1: PWM0 占空比 計數溢出值
RW RW-0XFFFF
*/

extern volatile uint32_t TMB2CR2 ;//0x40C34))

/*
 * TMB2CR1(TMB2 Control Register 2)
Bit [31:24] [23] [22] [21:20] [19:16]
名稱 - CPI3R RSV CPI3S RSV
RW - RW-0 - RW-0 -
Bit [15:00]
名稱 RSV
RW R-0
 */
extern volatile uint32_t TMCCR0 ;//0x40C14))

extern volatile uint32_t TMCCR1 ;//0x40C18))


/*
 * TMA Base Address + 0X18 (0X40C18)
Symbol TMCCR1(TMC Control Register 1)
Bit [31:16]
名稱 TCR1
RW R-X
Bit [15:00]
名稱 TCR0
RW R-X
 */

extern volatile uint32_t PT1CR0 ;//0x40800))
/*
 * PT1CR0 (PT1 Control Register 0)
Bit [31:24] [23] [22] [21] [20] [19] [18] [17] [16]
名稱 MASK PT1PU7 PT1PU6 PT1PU5 PT1PU4 PT1PU3 PT1PU2 PT1PU1 PT1PU0
RW R0W-0 RW-0
Bit [15:08] [7] [6] [5] [4] [3] [2] [1] [0]
名稱 MASK PT1OE7 PT1OE6 PT1OE5 PT1OE4 PT1OE3 PT1OE2 PT1OE1 PT1OE0
RW R0W-0 RW-0
 */

extern volatile uint32_t PT1CR1 ;//0x40804))

extern volatile uint16_t PT1CR1LW ;// 0x40804))

/*
 * PT1CR1 (PT1 Control Register 1)
Bit [31:24] [23] [22] [21] [20] [19] [18] [17] [16]
名稱 MASK PT1IE7 PT1IE6 PT1IE5 PT1IE4 PT1IE3 PT1IE2 PT1IE1 PT1IE0
RW R0W-0 RW-0
Bit [15:08] [7] [6] [5] [4] [3] [2] [1] [0]
名稱 MASK PT1DO7 PT1DO6 PT1DO5 PT1DO4 PT1DO3 PT1DO2 PT1DO1 PT1DO0
RW R0W-0 RW-0
 */
extern volatile uint32_t PT1CR2 ;//0x40808))

/*
 * PT1CR2 (PT1 Control Register 2)
Bit [31:16]
名稱 -
RW -
Bit [15:8] [7] [6] [5] [4] [3] [2] [1] [0]
名稱 - PT1DI[7] PT1DI[6] PT1DI[5] PT1DI[4] PT1DI[3] PT1DI[2] PT1DI[1] PT1DI[0]
RW - R-0
位元 名稱 描述
Bit[7~0] PT1DI
Port1 PAD 輸入狀態值
0 輸入低電位
1 輸入高電位
PT1DI: PT1 Data Input
 */
extern volatile uint32_t PT1CR3 ;//0x4080C))
/*
 * PT1CR3 (PT1 Control Register 3)
Bit [31:24] [23:21] [21:18] [17:16]
名稱 PT17IDF~ PT10IDF PT17ITT PT16ITT PT15ITT
RW R-0 RW-0
Bit [15] [14:12] [11:9] [8:6] [5:3] [2:0]
名稱 PT15ITT PT14ITT PT13ITT PT12ITT PT11ITT PT10ITT
RW RW-0

Bit[23~00] PT1#ITT
Port 10# 選擇中斷觸發方式. #代表的是0~7
000 關閉GPIO 中斷觸發，不能響應中斷
001 上升沿觸發
010 下降沿觸發
011 電位變化觸發
100 低電位觸發
101 高電位觸發
110 低電位觸發
111 高電位觸發

 */


extern volatile uint32_t PT2CR0 ;//0x40810))
extern volatile uint32_t PT2CR1 ;//0x40814))
extern volatile uint32_t PT2CR2 ;//0x40818))
extern volatile uint32_t PT2CR3 ;//0x4081C))

extern volatile uint32_t PT3CR0 ;//0x40820))
extern volatile uint32_t PT3CR1 ;//0x40824))
extern volatile uint32_t PT3CR2 ;//0x40828))
extern volatile uint32_t PT3CR3 ;//0x4082C))


// u16 format 


extern volatile uint16_t PT1PU ;// 0x40802))
extern volatile uint16_t PT1OE ;// 0x40800))
// prevent ambiguous, named INPE, input enable
extern volatile uint16_t PT1INPE ;// 0x40806)) 
extern volatile uint16_t PT1DO ;// 0x40804))
extern volatile uint8_t PT1DI  ;// 0x40808))
#define PT1INTMODE PT1CR3

extern volatile uint16_t PT2PU ;// 0x40812))
extern volatile uint16_t PT2OE ;// 0x40810))
// prevent ambiguous, named INPE, input enable
extern volatile uint16_t PT2INPE ;// 0x40816)) 
extern volatile uint16_t PT2DO ;// 0x40814))
extern volatile uint8_t PT2DI  ;// 0x40818))
#define PT2INTMODE PT2CR3


extern volatile uint16_t PT3PU ;// 0x40822))
extern volatile uint16_t PT3OE ;// 0x40820))
// prevent ambiguous, named INPE, input enable
extern volatile uint16_t PT3INPE ;// 0x40826)) 
extern volatile uint16_t PT3DO ;// 0x40824))
extern volatile uint8_t PT3DI  ;// 0x40828))
#define PT3INTMODE PT3CR3



extern volatile uint32_t PT601CFG ;//0x40850))
extern volatile uint32_t PT623CFG ;//0x40854))
extern volatile uint32_t PT645CFG ;//0x40858))
extern volatile uint32_t PT667CFG ;//0x4085C))

extern volatile uint32_t PT701CFG ;//0x40860))
extern volatile uint32_t PT723CFG ;//0x40864))
extern volatile uint32_t PT745CFG ;//0x40868))
extern volatile uint32_t PT767CFG ;//0x4086C))

extern volatile uint32_t PT801CFG ;//0x40870))
extern volatile uint32_t PT823CFG ;//0x40874))
extern volatile uint32_t PT845CFG ;//0x40878))
extern volatile uint32_t PT867CFG ;//0x4087C))

extern volatile uint32_t PT901CFG ;//0x40880))
extern volatile uint32_t PT923CFG ;//0x40884))
extern volatile uint32_t PT945CFG ;//0x40888))
extern volatile uint32_t PT967CFG ;//0x4088C))

extern volatile uint32_t PTA01CFG ;//0x40890))
extern volatile uint32_t PTA23CFG ;//0x40894))
extern volatile uint32_t PTA45CFG ;//0x40898))
extern volatile uint32_t PTA67CFG ;//0x4089C))

extern volatile uint32_t PTD01CFG ;//0x408C0))
extern volatile uint32_t PTD23CFG ;//0x408C4))
extern volatile uint32_t PTD45CFG ;//0x408C8))
extern volatile uint32_t PTD67CFG ;//0x408CC))

extern volatile uint32_t GPIOMCR01 ;//0x40840))
extern volatile uint32_t GPIOMCR23 ;//0x40844))

extern volatile uint16_t GPIOMCR2 ;// 0x40844))
extern volatile uint16_t GPIOMCR3 ;// 0x40846))


extern volatile uint32_t GPIOMCR45 ;//0x40848))
extern volatile uint32_t GPIOMCR67 ;//0x4084C))


extern volatile uint32_t ADCCR0 ;//0x41100))
extern volatile uint32_t ADCCR1 ;//0x41104))
extern volatile uint32_t ADCCR2 ;//0x41108))
extern volatile uint32_t ADCCR3 ;//0x4110C))
extern volatile uint32_t ADCCR4 ;//0x41110))

extern volatile uint32_t SPICR0 ;//0x40F00))
extern volatile uint32_t SPICR1 ;//0x40F04))
extern volatile uint32_t SPICR2 ;//0x40F08))
extern volatile uint32_t SPICR3 ;//0x40F0C))


extern volatile uint32_t UARTCR0 ;//0x40E00))
extern volatile uint32_t UARTCR1 ;//0x40E04))
extern volatile uint32_t UARTCR2 ;//0x40E08))
extern volatile uint32_t UARTCR3 ;//0x40E0C))

extern volatile uint16_t UARTTX  ;// 0x40E0E))
extern volatile uint16_t UARTRX  ;// 0x40E0C))

extern volatile uint32_t UART2CR0 ;//0x40E10))
extern volatile uint32_t UART2CR1 ;//0x40E14))
extern volatile uint32_t UART2CR2 ;//0x40E18))
extern volatile uint32_t UART2CR3 ;//0x40E1C))

extern volatile uint32_t I2CCR0 ;//0x41000))
extern volatile uint32_t I2CCR1 ;//0x41004))
extern volatile uint16_t I2CCR1H ;// 0x41006))
extern volatile uint16_t I2CCR1L ;// 0x41004))


extern volatile uint32_t I2CCR2 ;//0x41008))
extern volatile uint32_t I2CCR3 ;//0x4100C))
extern volatile uint32_t I2CCR4 ;//0x41010))
extern volatile uint16_t I2CCR4L ;// 0x41010))
extern volatile uint32_t I2CCR5 ;//0x41014))
extern volatile uint16_t I2CCR5L ;// 0x41014))



extern volatile uint32_t RTCCR0 ;//0x41A00))
extern volatile uint32_t RTCCR1 ;//0x41A04))
extern volatile uint32_t RTCCR2 ;//0x41A08))
extern volatile uint32_t RTCCR3 ;//0x41A0C))
extern volatile uint32_t RTCCR4 ;//0x41A10))
extern volatile uint32_t RTCCR5 ;//0x41A14))
extern volatile uint32_t RTCCR6 ;//0x41A18))
extern volatile uint32_t RTCCR7 ;//0x41A1C))

// alternative name of RTC

// RTCPMHR, PM at bit 6
extern volatile uint16_t RTCPMHR ;// 0x41A08))

extern volatile uint16_t RTCMIN ;// 0x41A0E))
extern volatile uint16_t RTCSEC ;// 0x41A0C))
extern volatile uint16_t RTCYEAR ;// 0x41A12))
extern volatile uint16_t RTCMONTH ;// 0x41A10))
extern volatile uint16_t RTCWDA ;// 0x41A14))
extern volatile uint16_t RTCDAT ;// 0x41A16))
// for alarm
// no mask
extern volatile uint16_t RTCA_PMHR ;// 0x41A1A))
// this has no mask
extern volatile uint16_t RTCA_MINSEC ;// 0x41A18))
extern volatile uint16_t RTCA_MONDAT ;// 0x41A1C))
extern volatile uint16_t RTCA_YEAR   ;// 0x41A1E))



extern volatile uint32_t LCDCR0 ;//0x41B00))
extern volatile uint32_t LCDCR1 ;//0x41B04))
extern volatile uint32_t LCDCR2 ;//0x41B08))








// bit 0: wait1
// bit 18..16: = 5 can proceed flash operation

#define FLAOP_MASE 1
#define FLAOP_SERA 2
#define FLAOP_WSINGLE 4
#define FLAOP_WMULTI 8
#define FLAOP_CRC 0x10
#define FLAOP_RSETTING 0x20

extern volatile uint32_t MEM_CFG ;//0x40600))
extern volatile uint16_t MEM_CFGH ;// 0x40602))
extern volatile uint16_t MEM_CFGL ;// 0x40600))
// flaop: {op_addr[15:0], count[7:0], 1'b0, 
// readProt, crc, write-multi, write-single, page-erase, mass-erase
extern volatile uint32_t MEM_FLAOP ;//0x40604))

extern volatile uint16_t MEM_FLAOPL ;// 0x40604))
extern volatile uint16_t MEM_FLAOPH ;// 0x40606))


// 0c: single write data or rambase word address[10:0] or crc-end addr [15:0]
extern volatile uint32_t MEM_FLAOPD ;//0x4060C))
// 10: crc result
extern volatile uint32_t MEM_FLACRC ;//0x40610))
// 11: flash password
extern volatile uint32_t MEM_FLAPWD ;//0x40614))





#endif /* HY16F391APB_H_ */

