// this file convert sd format back to old pat format
// for easy of verilog digital simulation


#include<stdio.h>
#include<string.h>
#include <ctype.h>
int main( int argc, char **argv)
{
    FILE *fp;
    char *sep;
    char buffer[1024];
    int repeat=1;
    int i=0, chk=0;
    if(argc<2)
    {
        fprintf(stderr,"ussage: %s <xx.sd>\n", argv[0]);
        return 1;
    }
    fp = fopen(argv[1], "r");
    if(fp==NULL)
    {
        fprintf(stderr,"file %s open error\n", argv[1]);
        return 2;
    }
    // skip until START:
    while(fgets(buffer,1023, fp))
    {
        i++;
        if(strstr(buffer,"START:"))
            break;
        
    }
    while(fgets(buffer,1023,fp))
    {
        int j=0,k=0;
        i++;
        if(strstr(buffer,"END:"))
            return 0;
        sep = strchr(buffer,';');
        if(!sep)
        {
            fprintf(stderr,"strange line %d:%s\n", i, buffer);
            return -1;
        }
        chk=sscanf(sep, ";RPT %d", &repeat);
        if(chk==0)
            repeat = 1;
	k=0;
        printf("(repeat %05d) \"",repeat);
        for(j=0;j<(int)(sep-buffer);j++)
        {
            if(isalnum(buffer[j]))
            {
                putchar(toupper(buffer[j]));
                k++;
            }
        }
        while(k<16)
        {
            putchar('X');
            k++;
        }
        printf("\"\n");
        
        
    }
    return 0;
}
