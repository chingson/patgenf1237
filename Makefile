
ifdef SystemRoot
EXE = f1237_patgen.EXE
SD2PAT = sd2pat.EXE
CC = cl.exe
RM = DEL /Q
else
EXE = "./f1237_patgen"
SD2PAT = "./sd2pat"
CC = gcc
RM = rm -f
endif

# all: atpgt.pat dig.pat eraseflash.pat ramt.pat romt.pat iot.pat atpgt.pat trim.pat
all: ramt.pat romt.pat iot.pat atpgt.pat flashlpoiddt.pat trimhao1.pat trimhao2.pat burn1.pat burnz5.pat burnwid.pat

%.pat: %.sd  $(SD2PAT)
	$(SD2PAT) $< > $@

$(SD2PAT): sd2pat.c
	$(CC) -O -o $@ $<

atpg0.sd: $(EXE) atpg.pat
	$< -a > $@

eraseflash.sd: $(EXE)
	$< -td -dskram -dskrom -dskio -dskatpg > $@

ramt.sd: $(EXE)
	$< -td -dskflasher -dskrom -dskio -dskatpg > $@

romt.sd: $(EXE)
	$< -td -dskflasher -dskram -dskio -dskatpg > $@

iot.sd: $(EXE)
	$< -td -dskflasher -dskram -dskrom -dskatpg > $@

atpgt.sd: $(EXE)
	$< -td -dskio -dskflasher -dskram -dskrom > $@


flashlpoiddt.sd: $(EXE) flashlposleep.bin 
	$< -noPT30CK -ramcode flashlposleep.bin 3072000 255 85 > $@

dig.sd: $(EXE)
	$< -td > $@

trim.sd: $(EXE)
	$< -tr > $@

trimhao1.sd: $(EXE) ramtrim1.bin 
	$< -ramcode ramtrim1.bin 102400 255 85 -noPT30CK -refck 10 377 > $@

trimhao2.sd: $(EXE) ramtrim2.bin 
	$< -ramcode ramtrim2.bin 102400 255 85 -noPT30CK -refck 10 49 > $@

burn1.sd: $(EXE) io1.bin
	$< -burncode io1.bin 8DA18DBF -password 1234 > $@

burnz5.sd: $(EXE) zero5.bin
	$< -burncode zero5.bin 2160C659 -password 1234 > $@

burnwid.sd: $(EXE)
	$< -wid B180C 2 16F3910 0F12371A > $@

$(EXE): f1237_patgen.c
	$(CC) -O -o $@ $<
	
clean:
	$(RM) $(SD2PAT) $(EXE) atpg0.pat dig.pat *.obj *.sd eraseflash.pat ramt.pat romt.pat iot.pat trim.pat atpgt.pat


