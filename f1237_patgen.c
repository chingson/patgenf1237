
// this is the C source code of  f1237_patgen,
// default is SD format
// which is used for V7

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>


//#include "HY391apb.h"


#define EDM_JTG_ID 0x1000063D
// idle for jtag run-idle
#define DEFAULT_IDLE_CYC 10
#define DEFAULT_EXEC1_CYC 42
// write mem when halt 
#define DEFAULT_EXEC2_CYC 30
// after pclk set to 4MHZ, fast w
#define DEFAULT_EXEC3_CYC 20
// PT3.0 is SCL/ECK
// PT3.1 is SDA/EDIO
// PT3.7 is ATPG clock

// RSTB PT1,2,3, 6~A, D, 9*8+1 = 73
#define CHAN_NUM 73
#define SCL_CHAN 17
#define SDA_CHAN 18
#define ECK_CHAN 17
#define EDIO_CHAN 18
#define ATPGCK_CHAN 24
#define RSTB_CHAN 0
// PT2.0 is uart wait
#define UARTWAIT 8

#define CMD_READ 1
#define CMD_WRITE 2
#define CMD_WMASK 3

// jtag state
#define JTGS_RESET   	1
#define JTGS_IDLE    	2
#define JTGS_SELDR   	3
#define JTGS_SELIR   	4
#define JTGS_CAPDR   	5
#define JTGS_SHIFTDR 	6
#define JTGS_EXIT1DR 	7
#define JTGS_PAUSEDR 	8
#define JTGS_EXIT2DR 	9
#define JTGS_UPDATEDR 	10
#define JTGS_CAPIR 		11
#define JTGS_SHIFTIR 	12
#define JTGS_EXIT1IR 	13
#define JTGS_PAUSEIR 	14
#define JTGS_EXIT2IR 	15
#define JTGS_UPDATEIR 	16

#define LL(x) ((uint64_t)x)


// access debug instruction memory
#define EDMIR_ACC_DIM 1
// access dbg system related system register
#define EDMIR_ACC_DBG_SR 2
// acess data transfer register
#define EDMIR_ACC_DTR 3
// access memory word
#define EDMIR_ACC_MEM_W 4
// access misc register
#define EDMIR_ACC_MISC_R 5
// fast access memory
#define EDMIR_FAST_ACCESS_M 6
// get debug event
#define EDMIR_GET_DBG_EVENT 7
// ice execute (from DIM, debug instruction memory)
#define EDMIR_EXECUTE 8
// basic ID
#define EDMIR_IDCODE 9
// ACCESS MEM half word, tdi->inc->rw->addr(31)->data(15?)->tdo, total 48 bit
#define EDMIR_ACC_MEM_H 10
// ACCESS MEM byte tdi->inc->rw->addr(32)->data(8)->tdo, total 42 bits
#define EDMIR_ACC_MEM_B 11
// BYPASS
#define EDMIR_BYPASS 15

// dr len for different ir
const int drlen_tab[16] = {
	-1, // ir 0 is invalid
	34, // ir=1, access DIM, inc,rw,data,
	41, // ir=2, dbg sr, inc,rw,addr,data, where addr 7 bits
	34, // ir=3,access dtr, inc,rw,data
	64, // ir=4, access mem w, inc, rw, addr, data, where addr 30 bits
	38, // ir=5, misc reg, inc, rw, addr, data, where addr has 4 bits
	33, // ir=6, fast access mem, inc, data, note that rw is fixed in SBAR
	4, // ir=7, get debug event, only 4 bits
	1, // ir=8, execute
	32, // ir=9, idcode
	49, // ir=10, acc half word
	42, // ir=11, acc byte
	-1, // ir=12,13,14 is invalid
	-1,
	-1,
	1 		// ir=15, bypass

};

// MISC registers
#define EDMMISC_DIMIR 0 //=	4'b0000
#define EDMMISC_SBAR 1 // =	4'b0001
#define EDMMISC_EDM_CMDR 2 //  =	4'b0010
#define EDMMISC_DBGER  3 //	4'b0011
#define EDMMISC_ACC_CTL 4 // = 	4'b0100
#define EDMMISC_EDM_PROBE 5 // =	4'b0101
#define EDMMISC_PASSCODE 6 // = 	4'b0110


#define EDMSR_BPC0 0 //	7'b0000000;
#define EDMSR_BPC1 1 //	7'b0000001;
#define EDMSR_BPC2 2 //	7'b0000010;
#define EDMSR_BPC3 3 //	7'b0000011;
#define EDMSR_BPC4 4 //	7'b0000100;
#define EDMSR_BPC5 5 //	7'b0000101;
#define EDMSR_BPC6 6 //	7'b0000110;
#define EDMSR_BPC7 7 //	7'b0000111;

#define EDMSR_BPA0 8 //	7'b0001000;
#define EDMSR_BPA1 9 //	7'b0001001;
#define EDMSR_BPA2 0xa //	7'b0001010;
#define EDMSR_BPA3 0xb //	7'b0001011;
#define EDMSR_BPA4 0xc //	7'b0001100;
#define EDMSR_BPA5 0xd //	7'b0001101;
#define EDMSR_BPA6 0xe //	7'b0001110;
#define EDMSR_BPA7 0xf //	7'b0001111;

#define EDMSR_BPAM0 0x10 //	7'b0010000;
#define EDMSR_BPAM1 0x11 //	7'b0010001;
#define EDMSR_BPAM2 0x12 //	7'b0010010;
#define EDMSR_BPAM3 0x13 //	7'b0010011;
#define EDMSR_BPAM4 0x14 //	7'b0010100;
#define EDMSR_BPAM5 0x15 //	7'b0010101;
#define EDMSR_BPAM6 0x16 //	7'b0010110;
#define EDMSR_BPAM7 0x17 //	7'b0010111;

#define EDMSR_BPV0 0x18 //	7'b0011000;
#define EDMSR_BPV1 0x19 //	7'b0011001;
#define EDMSR_BPV2 0x1A //	7'b0011010;
#define EDMSR_BPV3 0x1B //	7'b0011011;
#define EDMSR_BPV4 0x1C //	7'b0011100;
#define EDMSR_BPV5 0x1D //	7'b0011101;
#define EDMSR_BPV6 0x1E //	7'b0011110;
#define EDMSR_BPV7 0x1F //	7'b0011111;

#define EDMSR_EDM_CFG 0x28 //	7'b0101000;
#define EDMSR_EDMSW 0x30 //	7'b0110000;
#define EDMSR_EDM_CTL 0x38 //	7'b0111000;
#define EDMSR_EDM_DTR 0x40 //	7'b1000000;
#define EDMSR_EDM_PROBE 0x58 // 7'b1011000;
#define EDMSR_BPMTV 0x48 //	7'b1001000;
#define EDMSR_DIMBR 0x50 //	7'b1010000;



// some patterns disable optinally
int digGenEraseFlash = 1;
int digGenRAMTest = 1;
int digGenIOTest = 1;
int digGenROMTest = 1;
int digGenATPG = 1;


int genDigitalPatterns(void);
// we use global var here
int jtg_next_state(int nowState, int tms);
int edmInit(int waitCyc);
int edmShiftIR(int newir, int irlen); // currently ir length is 4 fixed, from runtest-idle
int edmShiftDR(uint64_t newdr, uint64_t olddrExp, uint64_t olddrMask, int drlen); // max is 64!!
int edmOutRow(int tms, int tdi, int exptdo, int nowState,int rpt); // exptdo=-1 means don't care

// exp means expect
int edmAccMiscReg(int rw, int addr, uint32_t dataoexp, uint32_t dataexpMask, int delayCyc);
int edmAccEdmSr(int rw, int addr, uint32_t dataoexp, uint32_t dataexpMask, int delayCyc);
int edmAccSingle(int rw, int addr, uint32_t dataoexp, uint32_t dataexpMask, int delayCyc);
int edmAccSingleHalf( int addr, uint32_t dataoexp,  int delayCyc); // it must be write
int edmAccMulti(int rw, int len, int addr, uint32_t *dataoexp, uint32_t dataexpMask, int delayCyc); // mask used globally
int genATPG(void); // through smbus
int genRAMCODE(char *filename, int giveclockcount, int waitPT1MASK, int waitPT1VAL, int mode); // run RAMCODE
int genBurnCode(void);
int genWID(void); // write ID
int showUssage(char *argv0);

int ramcodeClk=1024*64;
int ramcodePT1MASK=0;
int ramcodePT1V=0;

int jtg_state_now = JTGS_RESET;


#define TMS1CK jtg_state_now=edmOutRow(1,1,-1,jtg_state_now,1)
#define TMS0CK jtg_state_now=edmOutRow(0,1,-1,jtg_state_now,1)
#define TMS1CKN(x) jtg_state_now=edmOutRow(1,1,-1,jtg_state_now,x)
#define TMS0CKN(x) jtg_state_now=edmOutRow(0,1,-1,jtg_state_now,x)
#define TMS1DIO(di,tdo) jtg_state_now=edmOutRow(1,di,tdo,jtg_state_now,1)
#define TMS0DIO(di,tdo) jtg_state_now=edmOutRow(0,di,tdo,jtg_state_now,1)


// formats are list
int defaultRPT = 10;
int fastRPT = 2;
const int devID8 = 0xEA;
char patternSTR[100] ; // RSTB PT1,PT2,PT3, PT6~A, D, 48 chan more, later we will generate IO use pattern, total 73 channels for digital
void FHeadPNT(void);
void FEndPNT(void);
// this is normal mode, can be simulated with rtl code

double digf=6.0; // mhz
int needRAMCODE=0;
char *ramcodeFNAME;

int needClockChannel=0;
int clockChannel=0;
int clockChannelHalfPeriodCount=0;

int needBurnCode=0;
//char burnImage[131072+6144];
uint32_t burnImage[32768+1536];
int burnWordCount=0;
uint32_t crc32chk=0;
int usePT30CK=1; // default RAMCODE use PT30 as clock
int needPassword=0;
uint32_t password;

int needWID = 0; // need write id
int widAddr = 0;
int widNumber = 0;
uint32_t widDATA[1024];

static const uint32_t crc32_table[] =
    {
        0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9,
        0x130476dc, 0x17c56b6b, 0x1a864db2, 0x1e475005,
        0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61,
        0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd,
        0x4c11db70, 0x48d0c6c7, 0x4593e01e, 0x4152fda9,
        0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
        0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011,
        0x791d4014, 0x7ddc5da3, 0x709f7b7a, 0x745e66cd,
        0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
        0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5,
        0xbe2b5b58, 0xbaea46ef, 0xb7a96036, 0xb3687d81,
        0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
        0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49,
        0xc7361b4c, 0xc3f706fb, 0xceb42022, 0xca753d95,
        0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1,
        0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d,
        0x34867077, 0x30476dc0, 0x3d044b19, 0x39c556ae,
        0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
        0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16,
        0x018aeb13, 0x054bf6a4, 0x0808d07d, 0x0cc9cdca,
        0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,
        0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02,
        0x5e9f46bf, 0x5a5e5b08, 0x571d7dd1, 0x53dc6066,
        0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
        0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e,
        0xbfa1b04b, 0xbb60adfc, 0xb6238b25, 0xb2e29692,
        0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6,
        0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a,
        0xe0b41de7, 0xe4750050, 0xe9362689, 0xedf73b3e,
        0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
        0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686,
        0xd5b88683, 0xd1799b34, 0xdc3abded, 0xd8fba05a,
        0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637,
        0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb,
        0x4f040d56, 0x4bc510e1, 0x46863638, 0x42472b8f,
        0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
        0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47,
        0x36194d42, 0x32d850f5, 0x3f9b762c, 0x3b5a6b9b,
        0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
        0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623,
        0xf12f560e, 0xf5ee4bb9, 0xf8ad6d60, 0xfc6c70d7,
        0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
        0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f,
        0xc423cd6a, 0xc0e2d0dd, 0xcda1f604, 0xc960ebb3,
        0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7,
        0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b,
        0x9b3660c6, 0x9ff77d71, 0x92b45ba8, 0x9675461f,
        0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
        0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640,
        0x4e8ee645, 0x4a4ffbf2, 0x470cdd2b, 0x43cdc09c,
        0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,
        0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24,
        0x119b4be9, 0x155a565e, 0x18197087, 0x1cd86d30,
        0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
        0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088,
        0x2497d08d, 0x2056cd3a, 0x2d15ebe3, 0x29d4f654,
        0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0,
        0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c,
        0xe3a1cbc1, 0xe760d676, 0xea23f0af, 0xeee2ed18,
        0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
        0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0,
        0x9abc8bd5, 0x9e7d9662, 0x933eb0bb, 0x97ffad0c,
        0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668,
        0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4};

uint32_t
xcrc32(const unsigned char *buf, int len, uint32_t init)
{
  uint32_t crc = init;
  while (len--)
  {
    crc = (crc << 8) ^ crc32_table[((crc >> 24) ^ *buf) & 255];
    buf++;
  }
  return crc;
}


int output_line_no=1;

void FHeadPNT(void)
{
	printf("HEAD[PIN_NAME]:\n");
	printf("RSTB,PT10,PT11,PT12,PT13,PT14,PT15,PT16,PT17,PT20,PT21,PT22,PT23,PT24,PT25,PT26,PT27,PT30,PT31,PT32,PT33,PT34,PT35,PT36,PT37;\n");
	printf("END_HEAD;\n\n");
	printf("//\tRPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP\n");
	printf("//\tSTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT\n");
	printf("//\tT11111111222222223333333366666666777777778888888899999999AAAAAAAADDDDDDDD\n");
	printf("//\tB012345670123456701234567012345670123456701234567012345670123456701234567\n");
	printf("//\n");
	printf("@@PATTERN_DEFINE\n");
	printf("\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	printf("\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	printf("START:\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	printf("\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	printf("\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	output_line_no += 14;
}
void FEndPNT(void)
{
	printf("\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	printf("\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	printf("END:\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	printf("\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	printf("\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
	printf("@@END_PATTERN_DEFINE\n");
	output_line_no += 6;
}

#define i2cOutRow(a, b) i2cOutRowC(a, b, 0)

// *****************************************
// i2cOutRowC(scl,sda, checkd) has checkd means checkdata
// bit 0: sda check HL
// bit 1: scl drive high if set 1 and scl is 1
// bit 2: sda drive high if set 1 and sda is 1
// ****************************************

void i2cOutRowC(int scl, int sda, int checkd) // checkd means data chk
{
	if (checkd & 2)
		patternSTR[SCL_CHAN] = scl ? '1' : '0';
	else
		patternSTR[SCL_CHAN] = scl ? 'X' : '0';
	if (checkd & 1)
		patternSTR[SDA_CHAN] = sda ? 'H' : 'L';
	else if (checkd & 4)
		patternSTR[SDA_CHAN] = sda ? '1' : '0';
	else
		patternSTR[SDA_CHAN] = sda ? 'X' : '0';
	printf("	%s;RPT %2d;\n", patternSTR, defaultRPT);
	output_line_no ++;
}

void i2cOutStart(void)
{
	i2cOutRow(1, 1);
	i2cOutRow(1, 0);
	i2cOutRow(0, 0);
}
void i2cOutStop(void)
{
	i2cOutRow(0, 0);
	i2cOutRow(1, 0);
	i2cOutRow(1, 1);
}
void i2cOutByte(unsigned char byte)
{
	int i;
	int sda;
	for (i = 0x80; i; i >>= 1)
	{
		sda = (byte & i) ? 1 : 0;
		i2cOutRow(0, sda);
		i2cOutRow(1, sda);
		i2cOutRow(0, sda);
	}
}
void i2cOutAck(void)
{
	i2cOutRow(0, 0);
	i2cOutRow(1, 0);
	i2cOutRow(0, 0);
}
void i2cOutNack(void)
{
	i2cOutRow(0, 1);
	i2cOutRow(1, 1);
	i2cOutRow(0, 1);
}
void i2cAckChk(void)
{
	i2cOutRow(0, 1);
	i2cOutRowC(1, 0, 1); // means sda check low
	i2cOutRow(0, 1);	 // sda 1 is out X
}
void i2cReadByte(int checkv, int mask) // with mask
{
	int i;
	int sda;
	if (checkv < 0) // don't care value
	{
		for (i = 0; i < 8; i++)
		{
			i2cOutRow(0, 1);
			i2cOutRow(1, 1);
			i2cOutRow(0, 1);
		}
	}
	else
	{
		for (i = 0x80; i; i >>= 1)
		{
			i2cOutRow(0, 1);
			sda = 0;
			if (checkv & i)
				sda = 1;
			if (mask & i)
				i2cOutRowC(1, sda, 1);
			else
				i2cOutRowC(1, 1, 0); // SDA is X

			i2cOutRow(0, 1);
		}
	}
}
void i2cOutByteChkAck(unsigned char byte)
{
	i2cOutByte(byte);
	i2cAckChk();
}

void i2cOutRestart(void)
{
	i2cOutRow(0, 1);
	i2cOutRow(1, 1);
	i2cOutRow(1, 0);
	i2cOutRow(0, 0);
}

void i2cOutDelay(int cyc)
{
	int i;
	//printf(";delay %d\n",cyc);
	for (i = 0; i < cyc; i++)
		i2cOutRow(1, 1);
}

// gen resetb pulse
// it is pulled high internally, but .. drive will be faster
void GenRSTB(int cycle)
{
	
	patternSTR[RSTB_CHAN] = '0';
	if(cycle<=0)
		cycle=defaultRPT;
	printf("\t%s;RPT %2d;\n", patternSTR, cycle);
	patternSTR[RSTB_CHAN] = '1';
	printf("\t%s;\n", patternSTR);
	output_line_no +=2;
}

void i2cStartAddrWrite(void)
{
	i2cOutStart();
	i2cOutByte(devID8);
	i2cAckChk();
}

void i2cRestartAddrRead(void)
{
	i2cOutRestart();
	i2cOutByte(devID8 | 1);
	i2cAckChk();
}

void smbusWriteAddrWords(int count, uint32_t addr, uint32_t *wordp)
{
	int i;
	i2cStartAddrWrite();
	i2cOutByteChkAck(CMD_WRITE);
	i2cOutByteChkAck(addr & 0xff);
	i2cOutByteChkAck((addr >> 8) & 0xff);
	i2cOutByteChkAck((addr >> 16) & 0xff);
	for (i = 0; i < count; i++)
	{
		i2cOutByteChkAck(((*wordp) >> 0) & 0xff);
		i2cOutByteChkAck(((*wordp) >> 8) & 0xff);
		i2cOutByteChkAck(((*wordp) >> 16) & 0xff);
		i2cOutByteChkAck(((*wordp) >> 24) & 0xff);
		++wordp;
	}
	i2cOutStop();
}

// check mask is set globally
void smbusReadAddrBytes(int count, uint32_t addr, uint8_t *expect, uint8_t mask)
{
	int i;
	i2cStartAddrWrite();
	i2cOutByteChkAck(CMD_READ);
	i2cOutByteChkAck(addr & 0xff);
	i2cOutByteChkAck((addr >> 8) & 0xff);
	i2cOutByteChkAck((addr >> 16) & 0xff);
	i2cRestartAddrRead();
	for(i=0;i<count;i++)
	{
		i2cReadByte((int)*expect, mask);
	}
	i2cOutStop();
}
inline void dumpPattern(int rpt) 
{
	if(rpt!=1)
		printf("\t%s;RPT %d;\n",patternSTR,rpt);
	else
		printf("\t%s;\n",patternSTR);
	output_line_no ++;
}



// functions
// note that is is the function to 
// gen atpg from smbus, not EDM!!
// called from -a, not from -td
int genATPG(void)
{
	uint32_t wdata[1];
	FILE *atpgfp=NULL;
	char buf[1024];


	// default IO/changed
	// PT3.6 is tset_n, keep 1 for checking
	strncpy(patternSTR, "111111111XXXXXXXXXXXXXX10XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",sizeof(patternSTR)-1);
	FHeadPNT();
	GenRSTB(0); // first ir
	// 0x4061c write 0x06
	// 0x40600 write 0x808r
	wdata[0] = 0x00000006;
	smbusWriteAddrWords(1, 0x4061c, wdata);
	wdata[0] = 0x00008080;
	smbusWriteAddrWords(1, 0x40600, wdata);
	GenRSTB(0); // reset again
	patternSTR[ATPGCK_CHAN] = 'C'; // now give clock
	// now test mode should be enabled
	atpgfp = fopen("atpg.pat","r");
	if(atpgfp==NULL)
	{
		fprintf(stderr,"file %s cannot found/open, error.\n", "atpg.pat");
		return -3;
	}
	while(fgets(buf,sizeof(buf)-1, atpgfp)!=NULL)
	{
		// remove \r\n
		char *p1= strchr(buf,'\r');
		if(p1!=NULL)
			*p1='\0';
		p1=strchr(buf,'\n');
		if(p1!=NULL)
			*p1='\0';
		if(strlen(buf)<CHAN_NUM)
		{
			int i;
			int j = strlen(buf);
			for(i=j;i<CHAN_NUM;i++)
				buf[i]='X';
			buf[i]='\0';
		}
		printf("\t%s;\n",buf);
		output_line_no ++;
	}
	fclose(atpgfp);
	FEndPNT();
	return 0;
	
}


int main(int argc, char **argv)
{
	int i,j;
	int needGenDigPat = 0;
	uint32_t filecrc;
	FILE *fp;
	if (argc < 2)
	{
		showUssage(argv[0]);
		return -1;
	}
	for(i=1;i<argc;i++)
	{
		if(argv[i][0]!='-')
		{
			fprintf(stderr,"unknown argument %s\n", argv[i]);
			return -2;
		}
		switch(argv[i][1])
		{
			case 'r':
				if(!strcmp(argv[i]+2,"amcode") && i<(argc-4))
				{
					ramcodeFNAME=argv[++i];
					ramcodeClk = atoi(argv[++i]);
					ramcodePT1MASK = atoi(argv[++i]);
					ramcodePT1V = atoi(argv[++i]);
					needRAMCODE=1;
					break;
				}else if(!strncmp(argv[i]+2,"efc",3) && i < (argc-2))
				{
					needClockChannel=1;
					clockChannel = atoi(argv[++i]);
					clockChannelHalfPeriodCount = atoi(argv[++i]);
					break;
				}
				
				fprintf(stderr,"unknown argument %s\n", argv[i]);
				return -10;
		
				
			case 'i':
				if(!strcmp(argv[i]+2,"rpt") && i<(argc-1))
				{
					defaultRPT = atoi(argv[++i]);
					break;
				}
				fprintf(stderr,"unknown argument %s\n", argv[i]);
				return -3;
			case 'a': // ATPG by smbus, not preferred.
				return genATPG();
			case 'b':
				if(i<argc-2)
				{
					memset(burnImage, 0xff, sizeof(burnImage));
					fp = fopen(argv[++i],"rb");
					if(fp==NULL)
					{
						fprintf(stderr,"file %s open read error.\n", argv[i]);
						return -5;
					}
					j = fread((void*)burnImage,1, sizeof(burnImage), fp);
					fclose(fp);
					burnWordCount = ((j+7)/8)*2;
					needBurnCode = 1;
					if(argv[++i][0]=='-')
					{
						fprintf(stderr, "burn code need 2 params!!\n");
						return -5;
					}
					sscanf(argv[i],"%X",&crc32chk);

					fprintf(stderr,"check crc %X\n", crc32chk);
					filecrc = xcrc32((unsigned char *)burnImage, burnWordCount*4, 0xFFFFFFFFU);
					if(filecrc != crc32chk)
					{
						fprintf(stderr,"file crc is %X, but request is %X not same, give up.\n",
							filecrc, crc32chk);
						return -6;
					}
					break;
				}
				fprintf(stderr,"unknown argument %s\n", argv[i]);
				return -4;
			case 't':
				switch(argv[i][2])
				{
					case 'd': // digital
						//return genDigitalPatterns();
						needGenDigPat=1;
						break;
					case 'r': 
						genRAMCODE("./RAM_Code.bin",0,0,0, 0); // 0 means I2C	
						patternSTR[19] = 'C'; // now give clock
	// now test mode should be enabled
	// 16k run 500ms should be enough
						for(i=0;i<16000;i++)
						{
						// 162 ns extend to 16200ns, need repeat 100, that is, 50 low 50 high
							
							if( (i%8)<4)
								patternSTR[18] = '0'; // now give clock
							else
								patternSTR[18] = '1'; // now give clock

							patternSTR[19] = '0'; // now give clock
							printf("\t%s;RPT 50\n", patternSTR);
								
								patternSTR[19] = '1'; // now give clock
							printf("\t%s;RPT 50\n", patternSTR);
						}
						break;
					default:
						fprintf(stderr,"unknown argument %s\n", argv[i]);
						return -3;
				}
				break;

			case 'd':
				if(!strcmp(argv[i]+2,"skram"))
				{
					digGenRAMTest=0;
					fprintf(stderr,"dig skip ram \n");
					break;
				}
				if(!strcmp(argv[i]+2,"skrom"))
				{
					digGenROMTest=0;
					fprintf(stderr,"dig skip rom \n");
					break;
				}
				if(!strcmp(argv[i]+2,"skio"))
				{
					digGenIOTest=0;
					fprintf(stderr,"dig skip io \n");
					break;
				}
				if(!strcmp(argv[i]+2,"skflasher"))
				{
					digGenEraseFlash=0;
					fprintf(stderr,"dig skip flash Erase \n");
					break;
				}
				if(!strcmp(argv[i]+2,"skatpg"))
				{
					digGenATPG=0;
					fprintf(stderr,"dig skip ATPG \n");
					break;
				}
				if(!strcmp(argv[i]+2,"patf"))
				{
					if(i<argc-1)
					{
						digf=atof(argv[++i]);
						break;
					}
					else
						fprintf(stderr,"need float number for new freq after -dpatf.\n");
				}
				
				fprintf(stderr,"unknown argument %s\n", argv[i]);
				return -3;
			case 'n':
				if(argv[i][2]=='o')
							usePT30CK=0;
				break;
			case 'p':
				if(i<argc-1)
				{
					sscanf(argv[++i], "%X", &password);
					if(!password)
					{
						fprintf(stderr, "password 0 is not allowed, give up.\n");
						return -8;
					}
					needPassword = 1;
					break;
				}
				fprintf(stderr,"expect password after %s\n", argv[i]);
				return -9;
			case 'w': // only wid
				if(i<argc-3) // at least addr number data
				{
					if(sscanf(argv[++i], "%X", &widAddr)!=1)
					{
						fprintf(stderr,"wid address parse failed %s\n", argv[i]);
						return -10;
					}
					if(sscanf(argv[++i], "%d", &widNumber)!=1)
					{
						fprintf(stderr,"wid number parse failed %s\n", argv[i]);
						return -10;
					}
					for(j=0;j<widNumber;j++)
					{
						if(i>=argc-1)
						{
							fprintf(stderr,"wid expect more (%d)numbers of data.\n",widNumber);
							return -11;
						}
						if(sscanf(argv[++i],"%X", widDATA+j)!=1)
						{
							fprintf(stderr,"wid data parse fail %s\n", argv[i]);
							return -12;
						}
					}
					needWID=1;
					break;
				}
				fprintf(stderr,"expect [addr] [number of words] [data words] after %s\n", argv[i]);
				return -9;
				
			default:
			fprintf(stderr,"unknown argument %s\n", argv[i]);
			return -3;
		}
	}
	if(needGenDigPat)
		return genDigitalPatterns();
	if(needRAMCODE)
		return genRAMCODE(ramcodeFNAME,ramcodeClk, ramcodePT1MASK, ramcodePT1V, 1); // 1 means jtg
	if(needBurnCode)
		return genBurnCode();
	if(needWID)
		return genWID();

	return 0;
}
int jtg_next_state(int nowState, int tms)
{
	
	switch(nowState)
	{
		case JTGS_RESET   : return tms?JTGS_RESET:JTGS_IDLE;
		case JTGS_IDLE    : return tms?JTGS_SELDR:JTGS_IDLE;
		case JTGS_SELDR   : return tms?JTGS_SELIR:JTGS_CAPDR;
		case JTGS_SELIR   : return tms?JTGS_RESET:JTGS_CAPIR;
		case JTGS_CAPDR   : return tms?JTGS_EXIT1DR: JTGS_SHIFTDR;
		case JTGS_SHIFTDR : return tms?JTGS_EXIT1DR: JTGS_SHIFTDR;
		case JTGS_EXIT1DR : return tms?JTGS_UPDATEDR: JTGS_PAUSEDR;
		case JTGS_PAUSEDR : return tms?JTGS_EXIT2DR: JTGS_PAUSEDR;
		case JTGS_EXIT2DR : return tms?JTGS_UPDATEDR: JTGS_SHIFTDR;
		case JTGS_UPDATEDR : return tms?JTGS_SELDR: JTGS_IDLE;
		case JTGS_CAPIR   : return tms?JTGS_EXIT1IR: JTGS_SHIFTIR;
		case JTGS_SHIFTIR : return tms?JTGS_EXIT1IR: JTGS_SHIFTIR;
		case JTGS_EXIT1IR : return tms?JTGS_UPDATEIR: JTGS_PAUSEIR;
		case JTGS_PAUSEIR : return tms?JTGS_EXIT2IR: JTGS_PAUSEIR;
		case JTGS_EXIT2IR : return tms?JTGS_UPDATEIR: JTGS_SHIFTIR;
		case JTGS_UPDATEIR : return tms?JTGS_SELDR: JTGS_IDLE;
		default:
			fprintf(stderr,"unknown init state %d\n", nowState);
			exit(-1);
	}

	return -1;
}


int edmInit(int waitCyc)
{
	// longest DR is 64 bits,
	// shift out need 192 clocks,
	// that is, 200 tms high clock will force it comes to reset state

	int i;
	strncpy(patternSTR, "1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",sizeof(patternSTR)-1);
	patternSTR[ECK_CHAN]='0'; // initial no clock
	patternSTR[EDIO_CHAN]='1';
	jtg_state_now=JTGS_RESET;
	FHeadPNT();
	GenRSTB(0); // reset leave SMBUS state
	patternSTR[ECK_CHAN]='C'; // initial no clock

	jtg_state_now = edmOutRow(1,0,-1, jtg_state_now,500); // 200 TMS HIGH
	jtg_state_now = edmOutRow(0,0,-1,jtg_state_now, waitCyc); // now runtest-idle state (come here always?)
	


	edmAccMiscReg(1, EDMMISC_DBGER, 7, 0, DEFAULT_IDLE_CYC); // clear DBGER
	edmAccEdmSr(1, EDMSR_DIMBR, 0x00FF0000, 0, DEFAULT_EXEC1_CYC ); // give DIM_BR
	edmAccEdmSr(1, EDMSR_EDM_CTL, 0x80000000, 0,DEFAULT_EXEC1_CYC  ); // need r then w? 
	edmAccMiscReg(1, EDMMISC_EDM_CMDR, 0, 0, DEFAULT_EXEC1_CYC  ); // it should be halted

	return 0;

}

// return is next jtag state
int edmOutRow(int tms, int tdi, int exptdo, int nowState, int rpt) // exptdo=-1 means don't care
{
	// 3 phase is tdi, tms, tdo
	int i;
	if(nowState==JTGS_SHIFTDR || nowState==JTGS_SHIFTIR)
	{
		for(i=0;i<rpt;i++)
		{
			patternSTR[EDIO_CHAN]=tdi?'1':'0'; dumpPattern(1);
			patternSTR[EDIO_CHAN]=tms?'1':'0'; dumpPattern(1);
			patternSTR[EDIO_CHAN]= (exptdo<0)?'X':(exptdo?'H':'L'); dumpPattern(1);
			nowState= jtg_next_state(nowState,tms);
		}
	}
	else
	{
		patternSTR[EDIO_CHAN] = tms ? '1':'0'; dumpPattern(rpt);
		nowState= jtg_next_state(nowState,tms); // it must be at the same state!!
		if(rpt> 1 && nowState!=jtg_next_state(nowState,tms))
		{
			fprintf(stderr,"internal error, jtg state transition repeat fail at %s:%d\n", __FILE__,__LINE__);
			exit(-__LINE__);
		}

	}
	return nowState;
}

int edmShiftIR(int newir, int irlen) // currently ir length is 4 fixed, from runtest-idle
{
	// from runtest_idle, 
	int i;
	if(jtg_state_now!=JTGS_IDLE)
	{
		fprintf(stderr,"internal err!! shift ir not from idle. at %s:%d\n",__FILE__, __LINE__);
		exit(-__LINE__);
	}
	// tms = 1,1,0,0 will come to shift-ir
	TMS1CK;
	TMS1CK;
	TMS0CK;
	TMS0CK;
	for(i=0;i<irlen;i++)
	{
		if(i!=(irlen-1))
		{
			TMS0DIO((newir>>i)&1,-1);
		}else
		{
			TMS1DIO((newir>>i)&1,-1);
		}
	} // after the loop, comes to exit1ir
	TMS1CK; // after this clock, it comes to update ir
	TMS0CK; // after this clock, jtag state comes to runtest-idle
	return jtg_state_now;
}


int edmShiftDR(uint64_t newdr, uint64_t olddrExp, uint64_t olddrMask, int drlen) // max is 64!!
{
	int i;
	if(jtg_state_now!=JTGS_IDLE)
	{
		fprintf(stderr,"internal err!! shift dr not from idle. at %s:%d\n",__FILE__, __LINE__);
		exit(-__LINE__);
	}
	// tms = 1,0,0 comes to shift-dr state
	TMS1CK;
	TMS0CK;
	TMS0CK;
	for(i=0;i<drlen;i++)
	{
		int tdo = ((olddrMask>>i)&1)?((olddrExp>>i)&1):-1;
		if(i!=(drlen-1))
		{
			TMS0DIO((newdr>>i)&1, tdo);
		}else
		{
			TMS1DIO((newdr>>i)&1, tdo);
		}
	}// after this loop, state comes to exit1-dr
	TMS1CK; // update DR
	TMS0CK; // comes to runtest idle
	return jtg_state_now;
}


int edmAccMiscReg(int rw, int addr, uint32_t dataoexp, uint32_t dataexpMask, int delayCyc)
{
	int drlen = drlen_tab[EDMIR_ACC_MISC_R];
	uint64_t bitInC= 1LL<<(drlen-1); // issue & complete
	uint64_t bitRW=1LL<<(drlen-2);

	uint64_t tdi=bitInC | (LL(addr)<<32);
	uint64_t tdoExp=bitInC; // expect it should complete complete
	uint64_t data64ExpMask = bitInC | dataexpMask ;

	if(rw) // rw=1 means write, otherwise it is read
	{
		tdi=tdi|bitRW|dataoexp;
	}else
	{
		tdoExp |= dataoexp;
	}
	edmShiftIR(EDMIR_ACC_MISC_R, 4);
	edmShiftDR(tdi, tdoExp, 0ll, drlen); // first exp is 0ll
	TMS0CKN(delayCyc);
	tdi -= bitInC; // remove the inc bit
	edmShiftDR(tdi,tdoExp, data64ExpMask, drlen);
	return 0;
}
int edmAccEdmSr(int rw, int addr, uint32_t dataoexp, uint32_t dataexpMask, int delayCyc)
{
	int drlen = drlen_tab[EDMIR_ACC_DBG_SR];
	uint64_t bitInC= 1LL<<(drlen-1); // issue & complete
	uint64_t bitRW=1LL<<(drlen-2);

	uint64_t tdi=bitInC | (LL(addr)<<32);
	uint64_t tdoExp=bitInC; // expect it should complete complete
	uint64_t data64ExpMask = bitInC | dataexpMask ;

	if(rw) // rw=1 means write, otherwise it is read
	{
		tdi=tdi|bitRW|dataoexp;
	}else
	{
		tdoExp |= dataoexp;
	}
	edmShiftIR(EDMIR_ACC_DBG_SR, 4);
	edmShiftDR(tdi, tdoExp, 0ll, drlen); // first exp is 0ll
	TMS0CKN(delayCyc);
	tdi -= bitInC; // remove the inc bit
	edmShiftDR(tdi,tdoExp, data64ExpMask, drlen);
	return 0;
}
// apb, ilm, dlm
#define USE_APB edmAccMiscReg(1,EDMMISC_ACC_CTL, 0, 0, DEFAULT_IDLE_CYC)
#define USE_ILM edmAccMiscReg(1,EDMMISC_ACC_CTL, 1, 0, DEFAULT_IDLE_CYC)
#define USE_DLM edmAccMiscReg(1,EDMMISC_ACC_CTL, 2, 0, DEFAULT_IDLE_CYC)

int edmAccSingleHalf(int addr, uint32_t dataoexp, int delayCyc)
{
	int drlen = drlen_tab[EDMIR_ACC_MEM_W];
	uint64_t bitInC= 1LL<<(drlen-1); // issue & complete
	uint64_t bitRW=1LL<<(drlen-2);

	uint64_t tdi=bitInC | (LL(addr>>2)<<32);
	uint64_t tdoExp=bitInC; // expect it should complete complete
	int i;

	tdi=tdi|bitRW|dataoexp;
	edmShiftIR(EDMIR_ACC_MEM_W, 4);
	edmShiftDR(tdi, tdoExp, 0ll, drlen); // first exp is 0ll
	TMS0CKN(delayCyc);
	return 0;
	
}
int edmAccSingle(int rw, int addr, uint32_t dataoexp, uint32_t dataexpMask, int delayCyc)
{
	int drlen = drlen_tab[EDMIR_ACC_MEM_W];
	uint64_t bitInC= 1LL<<(drlen-1); // issue & complete
	uint64_t bitRW=1LL<<(drlen-2);

	uint64_t tdi=bitInC | (LL(addr>>2)<<32);
	uint64_t tdoExp=bitInC; // expect it should complete complete
	uint64_t data64ExpMask = bitInC | dataexpMask ;
	int i;

	if(rw) // rw=1 means write, otherwise it is read
	{
		tdi=tdi|bitRW|dataoexp;
	}else
	{
		tdoExp |= dataoexp;
	}
	edmShiftIR(EDMIR_ACC_MEM_W, 4);
	edmShiftDR(tdi, tdoExp, 0ll, drlen); // first exp is 0ll
	TMS0CKN(delayCyc);
	tdi -= bitInC; // remove the inc bit
	edmShiftDR(tdi,tdoExp, data64ExpMask, drlen);
	return 0;
	
}
int edmAccMulti(int rw, int len, int addr, uint32_t *dataoexp, uint32_t dataexpMask, int delayCyc) // mask used globally
{
	int drlen = drlen_tab[EDMIR_FAST_ACCESS_M];
	uint64_t bitInC= 1LL<<(drlen-1); // issue & complete
	

	uint64_t tdi;
	uint64_t tdoExp=bitInC; // expect it should complete complete
	uint64_t data64ExpMask = bitInC | dataexpMask ;
	int i,j;

	// set start address
	edmAccMiscReg(1, EDMMISC_SBAR, (addr&0xfffffffc)|(rw&1), 0, DEFAULT_IDLE_CYC);
	fprintf(stderr,"//write SBAR at line %d.\n", output_line_no);
	// now shift ir
	edmShiftIR(EDMIR_FAST_ACCESS_M,4);

	TMS0CKN(delayCyc);
	for(j=0;j<len;j++)
	{
		if(rw) // write
		{
			tdi=bitInC | dataoexp[j]; // no issue?
			edmShiftDR(tdi, 0,0, drlen);
			TMS0CKN(delayCyc); // don't know why it takes long?
			// fast acc will do the write directly, no need check complete
			//tdi-=bitInC;
			// need only make sure it is done
			//edmShiftDR(tdi,bitInC, bitInC, drlen);

		}else
		{
			tdi=bitInC; // other give 0 is OK
			if(j==0) // first, need issue, no check
				edmShiftDR(tdi,0,0,drlen);
			TMS0CKN(delayCyc);
			//tdi -=bitInC;
			tdoExp = bitInC|dataoexp[j];
			data64ExpMask = bitInC|dataexpMask;
			edmShiftDR(tdi,tdoExp,data64ExpMask, drlen);
		}

	}
	return 0;
}





int showUssage(char *argv0)
{
	fprintf(stderr, "Ussage: %s [options]:\n\
\t-td: Digital, JTAG->id->erase-fla->ram->rom->io->atpg \n\
\t-dskram: digital skip ram\n\
\t-dskram: digital skip ram\n\
\t-dskrom: digital skip rom\n\
\t-dskio: digital skip io\n\
\t-dskatpg: digital skip atpg\n\
\t-dskflasher: digital skip flash (info 6,7) erase \n\
\t-dpatf [digital pattern freq]: default is 6.0 MHz [RPT change for flash operation]\n\
\t-tr: generate trim pattern (by RAM_Code.bin)\n\
\t-irpt N: i2c (smbus row rpt #), default is 10.\n\
\t-ramcode [filename] [clkcnt] [expPT1mask] [expPT1V]: \n\
\t\tgenRAMCODE pattern to load. Give clock, check pt1 value (mask & value).\n\
\t-noPT30CK no use PT30 clock when genRAMCODE \n\
\t-refck [channel] [half-period-row-count]. When give ramcode, clock clkcnt \n\
\t\tgive channel 1/0 as reference clock\n\
\t-burn [image file] [crc32(hex)]: write an image file to flash, check crc\n\
\t-passw [password(hexnum)] : add password to protect flash after burn image (above)\n\
\t-wid [addr] [word num] [word0] [word1] ... [wordn-1]: write ID at (info6) addr, \n\
\t\t for example, at 0xB180C write 0x016F3910, 0x0F12371A, give \n\
\t\t -wid B180C 2 16F3910 0F12371A \n\
",
			argv0);
	return 0;
}

#define RAMSIZEW 2048
#define RAMPAT0 0x00FF00FF
#define RAMPAT1 0xFF00FF00
#define RAMPAT2 0x55AA55AA
#define RAMPAT3 0xAA55AA55

static void setRAMPAT(uint32_t *buf,uint32_t pat)
{
	int i=0;
	do {
		*buf++=pat;
		*buf++=pat^0xffffffff;
		i+=2;
	}while(i<RAMSIZEW);
}

int genDigitalPatterns(void)
{
	int i; 
	uint32_t ram_value[2048]; // 8kbyte = 2k word;
	edmInit(50); // wait until cpu runs, new version the issue will release the reset, we'll see

	// first we check id
	edmShiftIR(EDMIR_IDCODE, 4);
	edmShiftDR(0, EDM_JTG_ID, 0xFFFFFFFFull, 32);
	fprintf(stderr,"// get jtg id finish at line %d\n",output_line_no);
	USE_APB;
	fprintf(stderr,"// use APB at line %d\n",output_line_no);
	edmAccSingle(1, 0x4030C, 0x07010000,0,DEFAULT_EXEC2_CYC );
	fprintf(stderr,"// CLKCR3 set 0x07010000 (use PT30 as clock) at line %d\n",output_line_no);
	edmAccSingle(1, 0x40308, 0x18180000,0,DEFAULT_EXEC2_CYC );
	fprintf(stderr,"// CLKCR2 set 0x18180000 (use 4MHZ) at line %d\n",output_line_no);
	if(digGenEraseFlash) // first is erase flash, only info6 and info 7
	{
		fprintf(stderr,"// start FLASH INF erase at line %d\n",output_line_no);
		int sectorEraseDly = (int)(digf*2400.0); // a little more is fine
		// back door?
		edmAccSingle(1, 0x40614, 0x55193F61,0,DEFAULT_EXEC2_CYC );
		edmAccSingle(1, 0x40600, 0x07050700,0,DEFAULT_EXEC2_CYC );
		// delay time depends on the digital freq
		// rows = freq * 2ms 
		edmAccSingle(1, 0x40604, 0x86000002,0,sectorEraseDly ); //sector erase
		edmAccSingle(0, 0x40604, 0x00000000,0x000000ff,DEFAULT_EXEC2_CYC );
		edmAccSingle(1, 0x40604, 0x87000002,0,sectorEraseDly ); //sector erase
		edmAccSingle(0, 0x40604, 0x00000000,0x000000ff,DEFAULT_EXEC2_CYC );
		edmAccSingle(1, 0x4060C, 0x000087FF, 0,DEFAULT_EXEC2_CYC);
		edmAccSingle(1, 0x40604, 0x86000010,0,DEFAULT_EXEC2_CYC+40*512 );
		edmAccSingle(0, 0x40610, 0x01745503,0xffffffffu,DEFAULT_EXEC2_CYC );
		edmAccSingle(1, 0x40604, 0x00000020, 0,DEFAULT_EXEC2_CYC+50); // re-read prot
	}

	if(digGenROMTest)
	{
		fprintf(stderr,"// start ROM_TEST at  line %d\n",output_line_no);
		edmAccSingle(1, 0x4060C, 0x0000F5FF, 0,DEFAULT_EXEC2_CYC);
		edmAccSingle(1, 0x40604, 0xF0000010,0,DEFAULT_EXEC2_CYC+40*256*6 );
		edmAccSingle(0, 0x40604, 0x00000000,0x000000ff,DEFAULT_EXEC2_CYC );
		// ROM CRC is AE429E26
		edmAccSingle(0, 0x40610, 0xAE429E26,0xffffffffu,DEFAULT_EXEC2_CYC );
	}

	if(digGenRAMTest)
	{
	// next is testing RAM
		fprintf(stderr,"// start RAM_TEST at  line %d\n",output_line_no);
		USE_DLM;
		fprintf(stderr,"// use DLM at line %d\n",output_line_no);
		for(i=0;i<RAMSIZEW;i++)
			ram_value[i]=rand();
		edmAccMulti(1,RAMSIZEW, 0, ram_value, 0, DEFAULT_EXEC3_CYC);
		edmAccMulti(0,RAMSIZEW, 0, ram_value, 
			0xffffffffU, DEFAULT_EXEC3_CYC );
		fprintf(stderr,"// ram write/read random over at line %d\n",
			output_line_no);
		setRAMPAT(ram_value, RAMPAT0);
		edmAccMulti(1,RAMSIZEW, 0, ram_value, 0, DEFAULT_EXEC3_CYC);
		edmAccMulti(0,RAMSIZEW, 0, ram_value, 
			0xffffffffU, DEFAULT_EXEC3_CYC );
		fprintf(stderr,"// ram write/read %08X over at line %d\n",RAMPAT0,
			output_line_no);

		setRAMPAT(ram_value, RAMPAT1);
		edmAccMulti(1,RAMSIZEW, 0, ram_value, 0, DEFAULT_EXEC3_CYC);
		edmAccMulti(0,RAMSIZEW, 0, ram_value, 
			0xffffffffU, DEFAULT_EXEC3_CYC );
		fprintf(stderr,"// ram write/read %08X over at line %d\n",RAMPAT1,
			output_line_no);

		setRAMPAT(ram_value, RAMPAT2);
		edmAccMulti(1,RAMSIZEW, 0, ram_value, 0, DEFAULT_EXEC3_CYC);
		edmAccMulti(0,RAMSIZEW, 0, ram_value, 
			0xffffffffU, DEFAULT_EXEC3_CYC );
		fprintf(stderr,"// ram write/read %08X over at line %d\n",RAMPAT2,
			output_line_no);

		setRAMPAT(ram_value, RAMPAT3);
		edmAccMulti(1,RAMSIZEW, 0, ram_value, 0, DEFAULT_EXEC3_CYC);
		edmAccMulti(0,RAMSIZEW, 0, ram_value, 
			0xffffffffU, DEFAULT_EXEC3_CYC );
		fprintf(stderr,"// ram write/read %08X over at line %d\n",RAMPAT3,
			output_line_no);

	}
	if(digGenIOTest)
	{
		fprintf(stderr,"// start IO output test at line %d\n",output_line_no);
		// set all to 55
		// PT1 related registers PUOE, IEDO, DI, IDFITT
		USE_APB; // it is must
		edmAccSingle(1,0x40800,0xff00ffFF, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40804,0xffffff55, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40808,0x00000055, 0x000000ff, DEFAULT_EXEC2_CYC);
		// PT2
		edmAccSingle(1,0x40810,0xff00ffFF, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40814,0xffffff55, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40818,0x00000055, 0x000000ff, DEFAULT_EXEC2_CYC);
		// PT3
		edmAccSingle(1,0x40820,0xff00ffFC, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40824,0xffffff55, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40828,0x00000055, 0x000000fC, DEFAULT_EXEC2_CYC); // lowest lsb skip
		// PT6
		// 40850 is PT601, OE,IE,DO,DI	
		fprintf(stderr,"// PT655 test at line %d\n",output_line_no);
		edmAccSingle(1,0x40850,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40850,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40854,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40854,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40858,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40858,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4085C,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4085C,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		fprintf(stderr,"// PT755 test at line %d\n",output_line_no);
		// PT7
		edmAccSingle(1,0x40860,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40860,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40864,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40864,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40868,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40868,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4086C,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4086C,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);

		// PT8
		fprintf(stderr,"// PT855 test at line %d\n",output_line_no);
		edmAccSingle(1,0x40870,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40870,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40874,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40874,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40878,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40878,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4087C,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4087C,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);

		// PT9
		fprintf(stderr,"// PT955 test at line %d\n",output_line_no);
		edmAccSingle(1,0x40880,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40880,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40884,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40884,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40888,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40888,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4088C,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4088C,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);

		// PTA
		fprintf(stderr,"// PTA55 test at line %d\n",output_line_no);
		edmAccSingle(1,0x40890,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40890,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40894,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40894,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40898,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40898,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4089C,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4089C,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);

		// PTD
		fprintf(stderr,"// PTD55 test at line %d\n",output_line_no);
		edmAccSingle(1,0x408C0,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x408C0,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x408C4,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x408C4,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x408C8,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x408C8,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x408CC,0x0f0c0f0e, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x408CC,0x00000001, 0x00010001, DEFAULT_EXEC2_CYC);

		// test externally
//			   printf("\tXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;\n");
		strncpy(patternSTR, "1HLHLHLHLHLHLHLHLCXHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL",sizeof(patternSTR)-1);
		TMS0CK; // check once is enough
		strncpy(patternSTR, "1XXXXXXXXXXXXXXXXCXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",sizeof(patternSTR)-1);
		
		//edmAccSingle(1,0x40800,0xff00ffFF, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40804,0xffffffAA, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40808,0x000000AA, 0x000000ff, DEFAULT_EXEC2_CYC);
		// PT2
		//edmAccSingle(1,0x40810,0xff00ffFF, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40814,0xffffffAA, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40818,0x000000AA, 0x000000ff, DEFAULT_EXEC2_CYC);
		// PT3
		//edmAccSingle(1,0x40820,0xff00ffFC, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40824,0xffffffAA, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40828,0x000000AA, 0x000000fC, DEFAULT_EXEC2_CYC); // lowest lsb skip
		// PT6
		// 40850 is PT601, OE,IE,DO,DI	
		fprintf(stderr,"// PT655 test at line %d\n",output_line_no);
		edmAccSingle(1,0x40850,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40850,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40854,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40854,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40858,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40858,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4085C,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4085C,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		fprintf(stderr,"// PT755 test at line %d\n",output_line_no);
		// PT7
		edmAccSingle(1,0x40860,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40860,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40864,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40864,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40868,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40868,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4086C,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4086C,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);

		// PT8
		fprintf(stderr,"// PT855 test at line %d\n",output_line_no);
		edmAccSingle(1,0x40870,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40870,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40874,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40874,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40878,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40878,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4087C,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4087C,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);

		// PT9
		fprintf(stderr,"// PT955 test at line %d\n",output_line_no);
		edmAccSingle(1,0x40880,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40880,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40884,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40884,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40888,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40888,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4088C,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4088C,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);

		// PTA
		fprintf(stderr,"// PTA55 test at line %d\n",output_line_no);
		edmAccSingle(1,0x40890,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40890,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40894,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40894,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40898,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x40898,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x4089C,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x4089C,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);

		// PTD
		fprintf(stderr,"// PTD55 test at line %d\n",output_line_no);
		edmAccSingle(1,0x408C0,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x408C0,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x408C4,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x408C4,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x408C8,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x408C8,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x408CC,0x0f0e0f0c, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(0,0x408CC,0x00010000, 0x00010001, DEFAULT_EXEC2_CYC);
	
		strncpy(patternSTR, "1LHLHLHLHLHLHLHLHCXLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLH",sizeof(patternSTR)-1);
		TMS0CK; // check once is enough
		strncpy(patternSTR, "1XXXXXXXXXXXXXXXXCXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",sizeof(patternSTR)-1);
		edmAccSingle(1,0x40800,0xff00ff00, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40810,0xff00ff00, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40820,0xff00ff00, 0, DEFAULT_EXEC2_CYC);
		fprintf(stderr,"// IO test finish  at line %d\n",output_line_no);
		
	}
	if(digGenATPG)
	{
		FILE *atpgfp;
		char buf[1024];
		fprintf(stderr,"// atpg start test at line %d\n",output_line_no);
		USE_APB; // it is must
		edmAccSingle(1,0x4061C,0x00000006, 0, DEFAULT_EXEC2_CYC);
		edmAccSingleHalf(0x40600,0x00008080,DEFAULT_EXEC2_CYC);
		strncpy(patternSTR, "111111111XXXXXXXXXXXXXX10XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",sizeof(patternSTR)-1);
		GenRSTB(0); // reset again
		patternSTR[ATPGCK_CHAN] = 'C'; // now give clock
		atpgfp = fopen("atpg.pat","r");
		if(atpgfp==NULL)
		{
			fprintf(stderr,"file %s cannot found/open, error.\n", "atpg.pat");
			return -3;
		}
		while(fgets(buf,sizeof(buf)-1, atpgfp)!=NULL)
		{
			// remove \r\n
			char *p1= strchr(buf,'\r');
			if(p1!=NULL)
				*p1='\0';
			p1=strchr(buf,'\n');
			if(p1!=NULL)
				*p1='\0';
			if(strlen(buf)<CHAN_NUM)
			{
				int i;
				int j = strlen(buf);
				for(i=j;i<CHAN_NUM;i++)
					buf[i]='X';
				buf[i]='\0';
			}
			printf("\t%s;\n",buf);
			output_line_no ++;
		}
		fclose(atpgfp);

	}

	
	FEndPNT();
	return 0;
}



int genRAMCODE(char *filename, int giveClockCount, int chkPT1MASK, int chkPT1V, int mode)
{
	int i,len,j;
	char buf[4096]; // all are little endian
	uint32_t wdata[1];
	FILE *fp=fopen(filename,"rb"); // bin format
	memset(buf,0,sizeof(buf));
	if(fp==NULL)
	{
		fprintf(stderr,"error, file %s open read failed.\n", filename);
		exit(-3);
	}
	len = fread(buf,1,4096,fp);
	fclose(fp);
	j = (len+3)/4;
	strncpy(patternSTR, "1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",sizeof(patternSTR)-1);
	if(!mode)
	{
		FHeadPNT();
		GenRSTB(0); // first ir
	// 0x4061c write 0x06
	// 0x40600 write 0x808r
		wdata[0] = 0x00001716; // bit 4 is RAM as ROM
		smbusWriteAddrWords(1, 0x40600, wdata);

		smbusWriteAddrWords(j, 0x1000, (uint32_t*)buf);

	}else
	{
		// use jtag to write RAM
		edmInit(50); // wait until cpu runs, new version the issue will release the reset, we'll see

	// first we check id
		edmShiftIR(EDMIR_IDCODE, 4);
		edmShiftDR(0, EDM_JTG_ID, 0xFFFFFFFFull, 32);
		fprintf(stderr,"// get jtg id finish at line %d\n",output_line_no);
		USE_APB;
		fprintf(stderr,"// use APB at line %d\n",output_line_no);
		if(usePT30CK)
		{
			edmAccSingle(1, 0x4030C, 0x07010000,0,DEFAULT_EXEC2_CYC );
			fprintf(stderr,"// CLKCR3 set 0x07010000 (use PT30 as clock) at line %d\n",output_line_no);
		}
		edmAccSingle(1, 0x40308, 0x18180000,0,DEFAULT_EXEC2_CYC );
		fprintf(stderr,"// CLKCR2 set 0x18180000 (use 4MHZ) at line %d\n",output_line_no);
		USE_DLM;
		edmAccMulti(1,j, 0x1000,(uint32_t*)buf, 0, DEFAULT_EXEC2_CYC);
		USE_APB;
		edmAccSingle(1, 0x40600, 0x00001010, 0, DEFAULT_EXEC2_CYC); // set only bit4
		
	}
	
	GenRSTB(0); // reset again
	strncpy(patternSTR, "1XXXXXXXXXXXXXXXXCXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",sizeof(patternSTR)-1);
	if(needClockChannel)
		patternSTR[clockChannel]='0';
	while(giveClockCount)
	{
		if(needClockChannel)
		{

			if(giveClockCount>clockChannelHalfPeriodCount)
			{
				dumpPattern(clockChannelHalfPeriodCount);
				giveClockCount-=clockChannelHalfPeriodCount;
			}else
			{
				dumpPattern(giveClockCount);
				giveClockCount =0;
			}
			if(patternSTR[clockChannel]=='0')
				patternSTR[clockChannel]='1';
			else
			{
				patternSTR[clockChannel]='0';
			}
			

		}else
		{
			if(giveClockCount>1024)
			{
				dumpPattern(1024);
				giveClockCount-=1024;
			}else
			{
				dumpPattern(giveClockCount);
				giveClockCount =0;
			}
			
		}
		
	}
	// check the result
	for(i=1,j=1;i<0x100;i<<=1,j++)
	{

		if(chkPT1MASK&i)
		{
			patternSTR[j]=(chkPT1V&i)?'H':'L';
		}
	}
	dumpPattern(1);

	return 0;	
}

int genBurnCode(void) // use global var, return 0 if success
{
	int i,j;
	int massEraseDelay = (int) (digf*12000.0); // 12MS
	int writePageDelay ; // = (int) (digf*6144.0); // 5ms per page 256 word * 24us = 6144
	// internal 12 MHZ => 2.66 US per word
	int crcDelay = (int)( (float)(burnWordCount)*2.8 * digf);
	int writeDelay = (int)(30.*digf);
	edmInit(50);
	edmShiftIR(EDMIR_IDCODE, 4);
	edmShiftDR(0, EDM_JTG_ID, 0xFFFFFFFFull, 32);
	fprintf(stderr,"// get jtg id finish at line %d\n",output_line_no);
	USE_APB;
	fprintf(stderr,"// use APB at line %d\n",output_line_no);
	edmAccSingle(1, 0x40300, 0x0000FF19,0,DEFAULT_EXEC2_CYC );
	fprintf(stderr,"// CLKCR0 set 0xff19 at line %d\n",output_line_no);
	// 
	edmAccSingle(1, 0x40600, 0x07051F00,0,DEFAULT_EXEC2_CYC ); // unprot, ramcode
	// erase
	edmAccSingle(1, 0x40604, 0x00000001,0,massEraseDelay ); // erase
	for(i=0;i<burnWordCount;i+=256) // 256 words a time
	{
		// load ram 300
		if(burnWordCount-i > 256)
			j=256;
		else
		{
			j=burnWordCount-i; // how many to write
		}
		writePageDelay = (int)(digf*((float)j)*25);
		USE_DLM;
		edmAccMulti(1, j, 0x400, burnImage+i, 0, DEFAULT_EXEC2_CYC);
		USE_APB;
		edmAccSingle(1, 0x4060C, 0x0100, 0, DEFAULT_EXEC2_CYC); // word address
		edmAccSingle(1, 0x40604, (i<<16)|((j-1)<<8)|0x08, 0, writePageDelay);
	}
	// now CRC
	edmAccSingle(1, 0x4060C, burnWordCount-1, 0, DEFAULT_EXEC2_CYC);
	edmAccSingle(1, 0x40604, 0x00000010, 0, crcDelay);
	edmAccSingle(0, 0x40610, crc32chk, 0xffffffff, DEFAULT_EXEC2_CYC);

	if(needPassword)
	{
		edmAccSingle(1,0x4060C, password, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40604, 0x87FE0004, 0, writeDelay);
		edmAccSingle(1,0x4060C, 0x016f3910, 0, DEFAULT_EXEC2_CYC);
		edmAccSingle(1,0x40604, 0x87FF0004, 0, writeDelay);
		// RE-READ PROT
		edmAccSingle(1,0x40604, 0x00000020, 0, 200);
		// CHECK, expect full prot at bit 19
		edmAccSingle(0,0x40604, 0x00080000, 0x00080000, DEFAULT_EXEC2_CYC);

	}
	

	return 0;
}

int genWID(void) // write ID
{
	int i;
	int writeDelay=(int)(digf*30*widNumber) ;
	edmInit(50);
	edmShiftIR(EDMIR_IDCODE, 4);
	edmShiftDR(0, EDM_JTG_ID, 0xFFFFFFFFull, 32);
	fprintf(stderr,"// get jtg id finish at line %d\n",output_line_no);
	fprintf(stderr,"// use APB at line %d\n",output_line_no);
	edmAccSingle(1, 0x40300, 0x0000FF19,0,DEFAULT_EXEC2_CYC );
	fprintf(stderr,"// CLKCR0 set 0xff19 at line %d\n",output_line_no);

	USE_DLM;
	edmAccMulti(1, widNumber, 0x400, widDATA, 0, DEFAULT_EXEC2_CYC);
	USE_APB;
	edmAccSingle(1, 0x40600, 0x07051F00,0,DEFAULT_EXEC2_CYC ); // unprot, ramcode
	edmAccSingle(1, 0x40614, 0x55193F61,0,DEFAULT_EXEC2_CYC );
	edmAccSingle(1, 0x4060C, 0x00000100,0,DEFAULT_EXEC2_CYC ); // 400->100
	edmAccSingle(1, 0x40604, (((widAddr-0x90000)/4)<<16)|((widNumber-1)<<8)|0x08,0,
		writeDelay);
	edmAccSingle(1, 0x40600, 0x07001F00,0,DEFAULT_EXEC2_CYC ); // unprot, ramcode
	USE_ILM;	
	for(i=0;i<widNumber;i++)
		edmAccSingle(0, widAddr+i*4, widDATA[i], 0xffffffff, DEFAULT_EXEC2_CYC);
	return 0;
}